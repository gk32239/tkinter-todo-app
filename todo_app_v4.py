import tkinter as tk

class ToDoApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("Adil's ToDo")
        self.geometry("300x400")
        
        self.tasks = []
        
        self.task_var = tk.StringVar()
        self.task_entry = tk.Entry(self, textvariable=self.task_var)
        self.task_entry.pack()

        self.add_task_button = tk.Button(self, text="Add Task", command=self.add_task)
        self.add_task_button.pack()
        
        self.task_list = tk.Listbox(self)
        self.task_list.pack()
        
        self.remove_task_button = tk.Button(self, text="Remove Task", command=self.remove_task)
        self.remove_task_button.pack()
        
    def add_task(self):
        task = self.task_var.get()
        self.tasks.append(task)
        self.task_list.insert("end", task)
        self.task_var.set("")
        
    def remove_task(self):
        selected_task = self.task_list.get(self.task_list.curselection())
        self.tasks.remove(selected_task)
        self.task_list.delete(self.task_list.curselection())
        
if __name__ == "__main__":
    app = ToDoApp()
    app.mainloop()
