import tkinter as tk
from datetime import datetime


class ToDoApp(tk.Tk):
    def __init__(self):
        super().__init__()
        self.title("To-Do List")
        self.geometry("400x500")

        self.tasks = []
        self.task_var = tk.StringVar()
        self.priority_var = tk.StringVar()
        self.due_date_var = tk.StringVar()
      
        # Create widgets for entering task
        self.task_label = tk.Label(self, text="Task")
        self.task_label.grid(row=0, column=0, padx=5, pady=5)
        self.task_entry = tk.Entry(self, textvariable=self.task_var)
        self.task_entry.grid(row=0, column=1, padx=5, pady=5)
        self.task_remove = tk.Entry(self, textvariable=self.task_var)
        self.task_remove.grid(row=1, column=1, padx=5, pady=5)
        self.task_all_remove = tk.Entry(self, textvariable=self.task_var)
        self.task_all_remove.grid(row=1, column=1, padx=5, pady=5)

        # Create widgets for entering priority
        self.priority_label = tk.Label(self, text="Priority (1-3)")
        self.priority_label.grid(row=1, column=0, padx=5, pady=5)
        self.priority_entry = tk.Entry(self, textvariable=self.priority_var)
        self.priority_entry.grid(row=1, column=1, padx=5, pady=5)

        # Create widgets for entering due date
        self.due_date_label = tk.Label(self, text="Due date (dd-mm-yy)")
        self.due_date_label.grid(row=2, column=0, padx=5, pady=5)
        self.due_date_entry = tk.Entry(self, textvariable=self.due_date_var)
        self.due_date_entry.grid(row=2, column=1, padx=5, pady=5)

        self.add_task_button = tk.Button(self, text="Add Task", command=self.add_task)
        self.add_task_button.grid(row=3, column=0, columnspan=2, padx=5, pady=5)
        self.task_entry.bind('<Return>', self.add_task)
        
        self.remove_task_button = tk.Button(self, text="Remove Task", command=self.remove_task)
        self.remove_task_button.grid(row=3, column=1, columnspan=2, padx=5, pady=5)
        self.task_remove.bind('<Return>', self.remove_task)

        self.remove_all_task_button = tk.Button(self, text="Delete all task", command=self.clear_listbox)
        self.remove_all_task_button.grid(row=3, column=2, columnspan=2, padx=5, pady=5)
        self.task_all_remove.bind('<Return>', self.clear_listbox)

        self.add_task_button = tk.Button(self, text="Sort by Priority", command=self.sort_by_priority)
        self.add_task_button.grid(row=3, column=3, columnspan=2, padx=5, pady=5)
        self.task_entry.bind('<Return>', self.sort_by_priority)
        
        self.remove_task_button = tk.Button(self, text="Sort by Date", command=self.sort_by_date)
        self.remove_task_button.grid(row=3, column=4, columnspan=2, padx=5, pady=5)
        self.task_remove.bind('<Return>', self.sort_by_date)

        self.task_list = tk.Listbox(self)
        self.task_list.grid(row=5, column=2, columnspan=2, padx=5, pady=5, ipadx=200, ipady=200)


    def update_listbox(self,new_list_task):
        # clear the current listbox
        self.clear_listbox()
        # Populate listbox by appending each task to list
        for task in new_list_task:
            self.task_list.insert("end", task['task'])


    def clear_listbox(self):
        self.task_list.delete(0, "end")

    def add_task(self):
        task = self.task_var.get()
        date = self.due_date_var.get()
        priority = self.priority_var.get()
        self.tasks.append({"task_priority":priority, "task_due_date":date, "task":f'{task}   (Task Due Date---{date}) (Task Priority--> {priority} Star)'})
        self.task_list.insert("end", f'{task}   (Task Due Date---{date}) (Task Priority--> {priority} Star)')
        self.task_var.set("")
        self.due_date_var.set("")
        self.priority_var.set("")


    def remove_task(self):
        selected_task = self.task_list.get(self.task_list.curselection())
        # self.tasks.remove(selected_task)
        temp_task = self.tasks
        for index in range(len(temp_task)):
            if temp_task[index]['task'] == selected_task:
                self.tasks.pop(index)
                break
        # print('self.tasks--',self.tasks)
        self.task_list.delete(self.task_list.curselection())


    def sort_by_priority(self):
        newlist = sorted(self.tasks, key=lambda d: d['task_priority']) 
        self.update_listbox(newlist)


    def sort_by_date(self):
        newlist = self.tasks
        newlist.sort(key = lambda x: datetime.strptime(x['task_due_date'], '%d-%m-%Y'))
        self.update_listbox(newlist)


if __name__ == "__main__":
    app = ToDoApp()
    app.mainloop()
